import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { UserService } from 'src/app/services/user.service';
import { Author } from 'src/app/types/author';
import { Book } from 'src/app/types/book';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss']
})
export class BookCardComponent {

@Input() book!: Book;

  constructor(private bookService: BookService, private userService: UserService, private router: Router) { }

  @Output() delete = new EventEmitter();

  get userRole(): string | undefined {
    return this.userService.user?.role;
  }

  get available(): boolean {
    return this.book?.quantity > 0;
  }

  get authorFullNames(): String {
    let authorFullNames = '';
    this.book.authors.forEach(author => {
      authorFullNames += this.getAuthorFullName(author) + ', ';
    });
    return authorFullNames.slice(0, -2);
  }

  getAuthorFullName(author: Author): String {
    let authorFullName = author.firstName + ' ';
    if (author.middleName) {
      authorFullName += author.middleName + ' ';
    }
    return authorFullName + author.lastName;
  }

  onDelete(event: MouseEvent): void {
    event.stopPropagation();
    this.bookService.deleteBook(this.book.id).subscribe(() => {
      this.delete.emit(this.book.id);
    });
  }

  onClick(): void {
    this.router.navigate(['/book/' + this.book.id]);
  }

  rentBook() {
    this.bookService.rentBook(this.book.id).subscribe(() => {
      this.router.navigate(['/profile']);
    })
  }

}
