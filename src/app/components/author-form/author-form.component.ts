import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { BookService } from 'src/app/services/book.service';
import { Author } from 'src/app/types/author';

@Component({
  selector: 'app-author-form',
  templateUrl: './author-form.component.html',
  styleUrls: ['./author-form.component.scss']
})
export class AuthorFormComponent {

  authorForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    middleName: new FormControl(''),
    lastName: new FormControl('', Validators.required)
  })

  createdAuthor: Subject<Author> = new Subject();

  constructor(private bookService: BookService) { }

  submitAuthor() {
    const author = {
      firstName: this.authorForm.controls['firstName'].value,
      middleName: this.authorForm.controls['middleName'].value,
      lastName: this.authorForm.controls['lastName'].value
    }

    this.bookService.createAuthor(author).subscribe(author => {
      this.createdAuthor.next(author);
      this.authorForm.reset()
    });

  }

}
