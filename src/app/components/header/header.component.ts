import { Component } from '@angular/core';
import { DropdownItem } from 'src/app/types/dropdown-item';
import { User } from '../../types/user';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Role } from 'src/app/types/role';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  constructor(private userService: UserService, private router: Router) { }

  get user(): User | null {
    return this.userService.user;
  }

  get username(): string {
    return this.userService.username
  }

  get dropdownItems(): DropdownItem[] {
    let items: DropdownItem[] = []
    items.push({ text: 'Profile', link: 'profile' });
    if (this.user?.role === Role.admin) {
      items.push({ text: 'Rented books', link: 'rentals' });
      items.push({ text: 'Users', link: 'users' });
    }
    items.push({ text: 'Log out', action: this.logout })

    return items;
  }

  logout(): void {
    window.location.href = '/';
    localStorage.removeItem('jwt');
  }

}
