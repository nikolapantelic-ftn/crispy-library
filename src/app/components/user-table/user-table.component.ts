import { Component, ComponentRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Role } from 'src/app/types/role';
import { User } from 'src/app/types/user';
import { UserFormComponent } from '../user-form/user-form.component';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  users!: User[];
  editUserId: number | null = 0;
  userCopy!: User
  editErrors = {
    username: false,
    firstName: false,
    lastName: false
  };

  userFormComponent = UserFormComponent;


  ngOnInit(): void {
    this.userService.getAll().subscribe(users => {
      this.users = users;
    })
  }

  parseRole(role: Role) {
    return role === Role.user ? 'User' : 'Admin'
  }

  editUser(user: User) {
    this.clearErrors();
    if (this.editUserId) {
      this.cancelEdit();
    }
    this.userCopy = { ...user };
    this.editUserId = user.id;
  }

  saveUser(user: User) {
    if (!this.validateUser(user)) return;
    this.userService.update(user).subscribe(() => {
      this.editUserId = null;
    },
    error => {
      this.cancelEdit();
    });
  }

  navigateProfile(username: string): void {
    this.router.navigate(['/profile/' + username]);
  }

  private validateUser(user: User): boolean {
    let valid = true;
    if (!user.username) {
      this.editErrors['username'] = true;;
      valid = false;
    } else if (!user.firstName) {
      this.editErrors['firstName'] = true;
      valid = false;
    } else if (!user.lastName) {
      this.editErrors['lastName'] = true;
      valid = false;
    }
    return valid;
  }

  clearErrors(): void {
    this.editErrors = {
      username: false,
      firstName: false,
      lastName: false
    };
  }

  deleteUser(username: string) {
    this.userService.delete(username).subscribe(() => {
      const index = this.users.map(user => user.username).findIndex(usernameForDelete => username === usernameForDelete);
      this.users.splice(index, 1);
    })
  }

  onClick(event: MouseEvent) {
    event.stopPropagation();
  }

  cancelEdit() {
    const index = this.users.map(user => user.id).findIndex(id => id === this.editUserId);
    this.users[index] = this.userCopy;
    this.editUserId = null;
  }

  onRefLoaded(componentRef: ComponentRef<UserFormComponent>) {
    componentRef.instance.createdUser.subscribe(user => {
      this.users.push(user);
    })
  }

}
