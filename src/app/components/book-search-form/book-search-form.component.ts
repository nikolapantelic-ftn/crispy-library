import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-book-search-form',
  templateUrl: './book-search-form.component.html',
  styleUrls: ['./book-search-form.component.scss']
})
export class BookSearchFormComponent {

  @Output() search = new EventEmitter();
  @Output() sort = new EventEmitter();

  constructor() { }

  onSearch(event: any): void {
    this.search.emit(event);
  }

  onSort(event: any): void {
    this.sort.emit(event);
  }

}
