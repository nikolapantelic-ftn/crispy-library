import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { BookService } from 'src/app/services/book.service';
import { UserService } from 'src/app/services/user.service';
import { RentedBook } from 'src/app/types/rented-book';
import { Role } from 'src/app/types/role';

@Component({
  selector: 'app-rental-table',
  templateUrl: './rental-table.component.html',
  styleUrls: ['./rental-table.component.scss']
})
export class RentalTableComponent implements OnChanges {

  @Input() rentedBooks!: RentedBook[];

  constructor(private userService: UserService, private bookService: BookService, private router: Router) { }

  get isUserAdmin() {
    return this.userService.user?.role === Role.admin;
  }

  ngOnChanges(): void {
    if (this.rentedBooks) {
      this.sortRentedBooks();
    }
  }

  isOverdue(rentedBook: RentedBook): boolean {
    if (new Date() > new Date(rentedBook.returnDate)) {
      return true;
    }
    return false;
  }

  returnBook(rentedBook: RentedBook) {
    this.bookService.returnBook(rentedBook.id).subscribe(() => {
      const index = this.rentedBooks.map(rentedBook => rentedBook.id).findIndex(id => rentedBook.id === id);
      this.rentedBooks.splice(index, 1);
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    })
  }

  sortRentedBooks(): void {
    let date1: Date;
    let date2: Date;
    this.rentedBooks = this.rentedBooks.sort((book1, book2) => {
      date1 = new Date(book1.returnDate);
      date2 = new Date(book2.returnDate);
      if (date1 < date2) {
        return -1
      }
      if (date1 > date2) {
        return 1;
      }
      return 0;
    })
  }

}
