import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { BookService } from 'src/app/services/book.service';
import { UserService } from 'src/app/services/user.service';
import { Book } from 'src/app/types/book';
import { Role } from 'src/app/types/role';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.scss']
})
export class BookDetailsComponent implements OnInit {

  book!: Book;

  constructor(private bookService: BookService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) { }

  get available(): boolean {
    return this.book?.quantity > 0;
  }

  get isUserAdmin(): boolean {
    return this.userService.user?.role === Role.admin;
  }

  get authors(): string {
    let authors = '';
    this.book?.authors.forEach(author => {
      authors += author.firstName + ' ' + author.middleName + ' ' + author.lastName + ', ';
    })
    return authors.slice(0, -2);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.getBook(params.id);
      }
    })
  }

  getBook(id: number): void {
    this.bookService.getById(id).subscribe(book => {
      this.book = book;
    });
  }

  onDelete() {
    this.bookService.deleteBook(this.book.id).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

  rentBook() {
    this.bookService.rentBook(this.book.id).subscribe(() => {
      this.router.navigate(['/profile']);
    })
  }

}
