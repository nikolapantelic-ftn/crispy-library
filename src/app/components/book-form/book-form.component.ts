import { Component, ComponentRef, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { Author } from 'src/app/types/author';
import { Book } from 'src/app/types/book';
import { NewBook } from 'src/app/types/new-book';
import { environment } from 'src/environments/environment';
import { AuthorFormComponent } from '../author-form/author-form.component';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  private isbnRegex = /(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/;

  bookForm = new FormGroup({
    title: new FormControl('', Validators.required),
    subtitle: new FormControl(''),
    creationDate: new FormControl('', Validators.required),
    isbn: new FormControl('', [Validators.pattern(this.isbnRegex), Validators.required]),
    quantity: new FormControl('', [Validators.min(0), Validators.required]),
    imageUrl: new FormControl('')
  })

  bookId: number | null = null;

  searchField = new FormControl('');

  filteredAuthors: Author[] = []
  selectedImage = new FormData();
  selectedAuthors: Author[] = [];
  authors: Author[] = [];
  imgSrc: string = '';
  authorFormComponent = AuthorFormComponent;
  authorFormComponentRef!: ComponentRef<AuthorFormComponent>;

  constructor(private bookService: BookService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.bookService.fetchAuthors().subscribe(authors => {
      this.authors = authors;
      this.filterAuthors();
      this.route.params.subscribe(params => {
        this.bookId = params.id;
        if (this.bookId) {
          this.getBookForUpdate(this.bookId);
        }
      });
    });
  }

  getBookForUpdate(id: number): void {
    this.bookService.getById(id).subscribe((book: Book) => {
      this.bookForm.controls['title'].setValue(book.title);
      this.bookForm.controls['subtitle'].setValue(book.subtitle);
      this.bookForm.controls['creationDate'].setValue(book.creationDate);
      this.bookForm.controls['isbn'].setValue(book.isbn);
      this.bookForm.controls['quantity'].setValue(book.quantity);
      this.bookForm.controls['imageUrl'].setValue(book.imageUrl);
      this.authors.forEach(author => {
        if (book.authors.map(bookAuthor => bookAuthor.id).includes(author.id)) {
          this.select(author);
        }
      })
    })
  }

  getFullName(author: Author): string {
    let fullName = author.firstName + ' ';
    if (author.middleName) {
      fullName += author.middleName + ' ';
    }
    fullName += author.lastName;
    return fullName;
  }

  isSelected(author: Author) {
    return this.selectedAuthors.includes(author);
  }

  select(author: Author) {
    if (this.isSelected(author)) {
      const index = this.selectedAuthors.findIndex(a => a === author);
      this.selectedAuthors.splice(index, 1);
    } else {
      this.selectedAuthors.push(author);
    }
  }

  onFileChanged(event: any): void {
    const file = event.target.files[0];
    this.selectedImage = new FormData();
    this.selectedImage.append('image', file);
    

    const reader = new FileReader();
    reader.onload = e => {
      if (typeof e.target?.result === 'string')
      this.imgSrc = e.target?.result;
    }

    reader.readAsDataURL(file);
  }

  submitBook(): void {
    const newBook : NewBook = {
      title: this.bookForm.controls['title'].value,
      subtitle: this.bookForm.controls['subtitle'].value,
      authors: this.selectedAuthors,
      creationDate: this.bookForm.controls['creationDate'].value,
      isbn: this.bookForm.controls['isbn'].value,
      quantity: this.bookForm.controls['quantity'].value,
      imageUrl: this.bookForm.controls['imageUrl'].value
    }

    if (newBook.imageUrl) {
      this.submitWithoutImageUpload(newBook);
    } else {
      this.submitWithImageUpload(newBook);
    }
  }

  submitWithImageUpload(newBook: NewBook) {
    this.bookService.uploadImage(this.selectedImage).subscribe(imageUrl => {
      newBook.imageUrl = environment.host + '/content/' + imageUrl;
      this.sendBook(newBook);
    });
  }

  submitWithoutImageUpload(newBook: NewBook) {
    this.sendBook(newBook);
  }

  sendBook(newBook: NewBook) {
    if (this.bookId) {
      this.bookService.updateBook(this.bookId, newBook);
    } else {
      this.bookService.createBook(newBook);
    }
  }

  filterAuthors(): void {
    const search = this.searchField.value;
    if (search === '') {
      this.filteredAuthors = this.authors;
      return;
    }
    this.filteredAuthors = this.authors.filter(author => {
      const fullName = this.getFullName(author);
      if (fullName.toLowerCase().includes(search)) {
        return true;
      }
      return false;
    })
  }

  onRefLoaded(componentRef: ComponentRef<AuthorFormComponent>) {
    componentRef.instance.createdAuthor.subscribe(author => {
      this.authors.push(author);
      this.filterAuthors();
      this.select(author);
    })
  }

}
