import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, throwError } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { NewUser } from 'src/app/types/new-user';
import { User } from 'src/app/types/user';
import Validation from './validation';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent {

  userForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required)
  }, [Validation.match('password', 'confirmPassword')]);

  createdUser = new Subject<User>();

  constructor(private userService: UserService, private router: Router) { }

  submitUser(): void {
    const newUser: NewUser = this.userForm.value;
    this.userService.create(newUser).subscribe(user => {
      this.createdUser.next(user);
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    });
  }

}
