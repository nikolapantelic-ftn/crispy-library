import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { Router } from '@angular/router';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  })

  error = '';

  constructor(private service: UserService, private router: Router) { }

  onSubmit(): void {
    this.service.login({
      username: this.loginForm.controls['username'].value,
      password: this.loginForm.controls['password'].value
    }).subscribe(() => {
      this.router.navigate(['/']);
    },
    error => {
      this.error = error;
    });
  }
}
