import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-book-search-bar',
  templateUrl: './book-search-bar.component.html',
  styleUrls: ['./book-search-bar.component.scss']
})
export class BookSearchBarComponent {

  searchFormControl = new FormControl('');
  @Output() search = new EventEmitter();

  constructor() { }

  onSearch(): void {
    this.search.emit(this.searchFormControl.value);
  }

}
