import { Component, ComponentFactoryResolver, ComponentRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewContainerDirective } from './view-container.directive';

@Component({
  selector: 'app-modal-container',
  templateUrl: './modal-container.component.html',
  styleUrls: ['./modal-container.component.scss']
})
export class ModalContainerComponent implements OnInit {

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  @Input() title: string = '';
  @Input() component: any;
  @ViewChild(ViewContainerDirective, {static: true}) componentHost!: ViewContainerDirective;
  @Output() componentRef: Subject<ComponentRef<any>> = new Subject();

  ngOnInit(): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.component);
    const componentRef = this.componentHost.viewContainerRef.createComponent(componentFactory);
    this.componentRef.next(componentRef);
  }

}
