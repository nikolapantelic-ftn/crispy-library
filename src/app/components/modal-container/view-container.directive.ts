import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appComponentHost]'
})
export class ViewContainerDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
