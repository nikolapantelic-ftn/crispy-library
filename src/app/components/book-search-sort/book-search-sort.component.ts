import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-book-search-sort',
  templateUrl: './book-search-sort.component.html',
  styleUrls: ['./book-search-sort.component.scss']
})
export class BookSearchSortComponent {

  sortFormControl = new FormControl('');
  @Output() sort = new EventEmitter();

  constructor() { }

  onSort(): void {
    this.sort.emit(this.sortFormControl.value);
  }

}
