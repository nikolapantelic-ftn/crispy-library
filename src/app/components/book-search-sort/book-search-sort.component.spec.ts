import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookSearchSortComponent } from './book-search-sort.component';

describe('BookSearchSortComponent', () => {
  let component: BookSearchSortComponent;
  let fixture: ComponentFixture<BookSearchSortComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookSearchSortComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookSearchSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
