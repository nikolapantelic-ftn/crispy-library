import { Component, Input } from '@angular/core';
import { Role } from 'src/app/types/role';
import { User } from 'src/app/types/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent {

  @Input() user!: User;

  constructor() { }

  parseRole(role: Role): string {
    return role === Role.user ? 'User' : 'Admin';
  }

}
