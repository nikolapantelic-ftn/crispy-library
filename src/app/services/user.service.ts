import { Injectable } from '@angular/core';
import { LoginRequest } from '../types/login-request';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../types/user';
import { Router } from '@angular/router';
import { NewUser } from '../types/new-user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private authUrl = environment.apiHost + '/auth';
  private usersUrl = environment.apiHost + '/users'; 

  constructor(private http: HttpClient, private jwtService: JwtHelperService, private router: Router) { }

  get user(): User | null {
    const jwt = localStorage.getItem('jwt');
    if (jwt) {
      return this.jwtService.decodeToken(jwt);
    }
    return null;
  }

  get username(): string {
    if (this.user?.username) {
      return this.user?.username;
    }
    return '';
  }

  get loggedIn(): boolean {
    return this.user ? true : false;
  }

  login(request: LoginRequest): Observable<HttpResponse<any>> {
    return this.http.post(this.authUrl, request, {observe: 'response'}).pipe(tap(response => {
      const jwt = response.headers.get('Authorization');
      if (jwt) {
        localStorage.setItem('jwt', jwt);
      }
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    }));
  }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  create(newUser: NewUser): Observable<User> {
    return this.http.post<User>(this.usersUrl, newUser);
  }

  update(user: User): Observable<User> {
    const url = this.usersUrl + '/' + user.id;
    return this.http.put<User>(url, user);
  }

  delete(username: string): Observable<any> {
    const url = this.usersUrl + '/' + username;
    return this.http.delete(url);
  }

  get(username: string): Observable<User> {
    const url = this.usersUrl + '/' + username;
    return this.http.get<User>(url);
  }

  getProfile(): Observable<User> {
    const url = this.usersUrl + '/profile';
    return this.http.get<User>(url);
  }
 
}
