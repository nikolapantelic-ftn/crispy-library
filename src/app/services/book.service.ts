import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators'
import { environment } from 'src/environments/environment';
import { Author } from '../types/author';
import { NewAuthor } from '../types/new-author';
import { NewBook } from '../types/new-book';
import { Book } from '../types/book';
import { map } from 'rxjs/operators';
import { RentedBook } from '../types/rented-book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private authorUrl = environment.apiHost + '/authors';
  private bookUrl = environment.apiHost + '/books';
  private imageUrl = environment.apiHost + '/images';
  private rentalUrl = environment.apiHost + '/rent'

  constructor(private http: HttpClient, private router: Router) { }

  fetchAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>(this.authorUrl);
  }

  createAuthor(author: NewAuthor): Observable<Author> {
    return this.http.post<Author>(this.authorUrl, author);
  }

  createBook(book: NewBook): void {
    this.http.post(this.bookUrl, book).subscribe(() => {
      this.router.navigate(['/']);
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    });
  }

  updateBook(id: number, book: NewBook) {
    const url = this.bookUrl + '/' + id;
    this.http.put(url, book).subscribe(() => {
      this.router.navigate(['/']);
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    });
  }

  deleteBook(id: number): Observable<any> {
    const url = this.bookUrl + '/' + id;
    return this.http.delete(url).pipe(tap(() => {},
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    }));
  }

  uploadImage(image: FormData): Observable<string> {
    return this.http.post(this.imageUrl, image, {responseType: 'text'}).pipe(catchError(error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    }));
  }

  get(page: number, searchText: string): Observable<Book[]> {
    let searchQuery = '';
    if (searchText != '') {
      searchQuery = '&title=' + searchText + '&subtitle=' + searchText + '&firstName=' + searchText + '&middleName=' + searchText + '&lastName=' + searchText;
    }
    return this.http.get<Book[]>(this.bookUrl + '?page=' + page + searchQuery);
  }

  getById(id: number): Observable<Book> {
    const url = this.bookUrl + '?id=' + id;
    return this.http.get<Book[]>(url).pipe(map(books => books[0]));
  }

  getRentalsByUser(username: string): Observable<RentedBook[]> {
    const url = this.rentalUrl + '/user/' + username;
    return this.http.get<RentedBook[]>(url);
  }

  getProfileRentals(): Observable<RentedBook[]> {
    const url = this.rentalUrl + '/personal';
    return this.http.get<RentedBook[]>(url);
  }

  returnBook(rentalId: number): Observable<any> {
    const url = this.rentalUrl + '/' + rentalId;
    return this.http.put(url, null);
  }

  rentBook(bookId: number): Observable<RentedBook> {
    return this.http.post<RentedBook>(this.rentalUrl, { bookId }).pipe(tap(() => {},
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    }));
  }

  getAllRentals(): Observable<RentedBook[]> {
    return this.http.get<RentedBook[]>(this.rentalUrl);
  }

}
