import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookFormComponent } from './components/book-form/book-form.component';
import { BookDetailsComponent } from './components/book-details/book-details.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { UsersComponent } from './pages/users/users.component';
import { RentedBooksComponent } from './pages/rented-books/rented-books.component';
import { LoginComponent } from './components/login/login.component';
import { BooksComponent } from './pages/books/books.component';
import { AdminGuard } from './services/guards/admin.guard';
import { UserGuard } from './services/guards/user.guard';
import { ErrorPageComponent } from './pages/error-page/error-page.component';

const routes: Routes = [
  { path: 'book/edit/:id', component: BookFormComponent, canActivate: [AdminGuard] },
  { path: 'book/new', component: BookFormComponent, canActivate: [AdminGuard] },
  { path: 'book/:id', component: BookDetailsComponent },
  { path: 'profile/:username', component: ProfileComponent, canActivate: [AdminGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [UserGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AdminGuard] },
  { path: 'rentals', component: RentedBooksComponent, canActivate: [AdminGuard] },
  { path: 'login', component: LoginComponent },
  { path: '', component: BooksComponent },
  { path: '**', component: ErrorPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
