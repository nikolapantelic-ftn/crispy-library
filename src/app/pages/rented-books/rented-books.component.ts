import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { BookService } from 'src/app/services/book.service';
import { RentedBook } from 'src/app/types/rented-book';

@Component({
  selector: 'app-rented-books',
  templateUrl: './rented-books.component.html',
  styleUrls: ['./rented-books.component.scss']
})
export class RentedBooksComponent implements OnInit {

  rentedBooks!: RentedBook[]

  constructor(private bookService: BookService, private router: Router) { }

  ngOnInit(): void {
    this.bookService.getAllRentals().subscribe(rentedBooks => {
      this.rentedBooks = rentedBooks;
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    })
  }

}
