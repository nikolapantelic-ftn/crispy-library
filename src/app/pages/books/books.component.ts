import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/types/book';
import { debounce } from 'lodash';
import { UserService } from 'src/app/services/user.service';
import { Role } from 'src/app/types/role';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  books: Book[] = [];
  page = 0;
  endReached = false;
  inProgress = false;
  searchText = '';
  sortType = '';

  constructor(private bookService: BookService, private userService: UserService) { }

  get isUserAdmin() {
    return this.userService.user?.role === Role.admin
  }

  ngOnInit(): void {
    this.getBooks();
    this.onSearch = debounce(this.onSearch, 600);
  }

  getBooks(): void {
    this.bookService.get(this.page, this.searchText).subscribe(books => {
      if (books.length !== 0) {
        this.books = this.books.concat(books);
        this.inProgress = false;
      } else {
        this.endReached = true;
      }
    });
  }

  onScroll(): void {
    if (this.endReached) return;
    const maxScrollY = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    const scrollPercentage = window.scrollY / maxScrollY * 100;
    if (scrollPercentage > 80 && !this.inProgress) {
      this.inProgress = true;
      this.page++;
      this.getBooks()
    }
  }

  onSearch(event: any) {
    this.searchText = event;
    this.books = [];
    this.page = 0;
    this.getBooks();
  }

  onSort(event:any) {
    this.sortType = event;
    this.sortBooks();
  }

  sortBooks(): void {
    if (this.sortType === 'title') {
      this.sortByTitle();
    } else if (this.sortType === 'creation-date') {
      this.sortByCreationDate();
    }
  }

  sortByTitle(): void {
    this.books = this.books.sort((b1, b2) => b1.title.localeCompare(b2.title));
  }

  sortByCreationDate(): void {
    let date1, date2;
    this.books = this.books.sort((b1, b2) => {
      date1 = new Date(b1.creationDate);
      date2 = new Date(b2.creationDate);
      if (date1 < date2) return -1
      else if (date1 > date2) return 1;
      else return 0;
    });
  }

  removeBook(id: number) {
    const index = this.books.map(book => book.id).findIndex(bookId => bookId === id);
    this.books.splice(index, 1);
  }
}
