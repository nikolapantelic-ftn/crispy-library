import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { BookService } from 'src/app/services/book.service';
import { UserService } from 'src/app/services/user.service';
import { RentedBook } from 'src/app/types/rented-book';
import { Role } from 'src/app/types/role';
import { User } from 'src/app/types/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user!: User;
  rentedBooks!: RentedBook[];

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, private bookService: BookService) { }

  get isRoleAdmin() {
    return this.userService.user?.role === Role.admin;
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.username) {
        this.getUser(params.username);
        this.getRentedBooks(params.username);
      } else if (this.userService.loggedIn) {
        this.getProfile();
        this.getProfileRentedBooks();
      }
    });
  }

  getRentedBooks(username: string): void {
    this.bookService.getRentalsByUser(username).subscribe(rentals => {
      this.rentedBooks = rentals;
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    });
  }

  getProfileRentedBooks(): void {
    this.bookService.getProfileRentals().subscribe(rentals => {
      this.rentedBooks = rentals;
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    });
  }

  getUser(username: string) {
    this.userService.get(username).subscribe(user => {
      this.user = user;
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    })
  }

  getProfile() {
    this.userService.getProfile().subscribe(user => {
      this.user = user;
    },
    error => {
      this.router.navigate(['/error'], {state: {error: error.error}});
      return throwError(error);
    })
  }

  isOverdue(rentedBook: RentedBook) {
    return new Date() > rentedBook.returnDate;
  }

}
