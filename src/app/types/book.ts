import { Author } from './author'

export interface Book {

  id: number
  title: string;
  subtitle: string;
  authors: Author[];
  creationDate: string;
  quantity: number;
  isbn: string;
  imageUrl: string;
  
}