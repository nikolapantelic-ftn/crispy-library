import { Author } from "./author";

export interface NewBook {
  title: string;
  subtitle: string;
  authors: Author[];
  creationDate: string;
  quantity: number;
  isbn: string;
  imageUrl: string;
}