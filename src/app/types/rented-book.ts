import { Book } from "./book";
import { User } from "./user";

export interface RentedBook {

  id: number;
  renter: User;
  book: Book;
  rentingDate: Date;
  returnDate: Date;
  daysRemaining: number;
  returned: boolean;

}