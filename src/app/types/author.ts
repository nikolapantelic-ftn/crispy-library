export interface Author {

  id: number;
  firstName: string,
  middleName: string | null;
  lastName: string;
  
}