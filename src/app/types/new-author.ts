export interface NewAuthor {

  firstName: string;
  middleName?: string;
  lastName: string;
  
}