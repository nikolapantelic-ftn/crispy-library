import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { BookDetailsComponent } from './components/book-details/book-details.component';
import { BooksComponent } from './pages/books/books.component';
import { UsersComponent } from './pages/users/users.component';
import { RentedBooksComponent } from './pages/rented-books/rented-books.component';
import { BookFormComponent } from './components/book-form/book-form.component';
import { HeaderComponent } from './components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms'
import { DropdownMenuComponent } from './components/dropdown-menu/dropdown-menu.component';
import { AppJwtModule } from './app-jwt.module';
import { ModalContainerComponent } from './components/modal-container/modal-container.component';
import { ViewContainerDirective } from './components/modal-container/view-container.directive';
import { AuthorFormComponent } from './components/author-form/author-form.component';
import { BookCardComponent } from './components/book-card/book-card.component';
import { BookSearchFormComponent } from './components/book-search-form/book-search-form.component';
import { BookSearchBarComponent } from './components/book-search-bar/book-search-bar.component';
import { BookSearchSortComponent } from './components/book-search-sort/book-search-sort.component';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { UserTableComponent } from './components/user-table/user-table.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { RentalTableComponent } from './components/rental-table/rental-table.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    BookDetailsComponent,
    BooksComponent,
    UsersComponent,
    RentedBooksComponent,
    BookFormComponent,
    HeaderComponent,
    DropdownMenuComponent,
    ModalContainerComponent,
    ViewContainerDirective,
    AuthorFormComponent,
    BookCardComponent,
    BookSearchFormComponent,
    BookSearchBarComponent,
    BookSearchSortComponent,
    ErrorPageComponent,
    UserTableComponent,
    UserFormComponent,
    UserDetailsComponent,
    RentalTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppJwtModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
